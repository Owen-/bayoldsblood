﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class CityBuilder : MonoBehaviour 
{
	public enum GenerationStyle { Orange, Egg, Grape }
	public GenerationStyle gStyle = GenerationStyle.Orange;
	public int generationValue;
	public float scale;

	public GameObject wall;
	public GameObject tower;
	public GameObject floor;
	public GameObject[] building;

	public bool build=false;

	private GameObject oldObj;	
	private List<Transform> perimeter;

	private void SetPerimeter()
	{
		perimeter=new List<Transform>();
		foreach(Transform child in transform)
		{
			perimeter.Add(child);
		}
	}

	private void Update()
	{
		if (build)
		{
			Build();
			build=false;
		}
	}


	private void Build()
	{
		oldObj=new GameObject("city");

		SetPerimeter();
		if(gStyle == GenerationStyle.Orange) GenerateOrange();
		if(gStyle == GenerationStyle.Egg) GenerateEgg();
		if(gStyle == GenerationStyle.Grape) GenerateGrape();
	}

	private void GenerateOrange()
	{
		GenerateWalls();
	}

	private void GenerateGrape()
	{

	}

	private void GenerateEgg()
	{
		GenerateWalls();
	}

	private void GenerateWalls()
	{
		Debug.Log(perimeter.Count);
		for (int i = 0; i< perimeter.Count; i++)
		{
			int second = i+1;
			if(i==perimeter.Count-1) second=0;

			GameObject t = (GameObject) Instantiate(tower, perimeter[i].position, Quaternion.identity);
			t.transform.position = new Vector3(t.transform.position.x, t.transform.localScale.y/2f, t.transform.position.z);

			GameObject w = (GameObject)Instantiate(wall, (perimeter[i].position+perimeter[second].position)/2f, Quaternion.identity);
			w.transform.LookAt(perimeter[i].position);
			w.transform.localScale = new Vector3(1f, 1f, (perimeter[i].position-perimeter[second].position).magnitude );

			w.transform.parent=oldObj.transform;
			t.transform.parent=oldObj.transform;

		}
	}
}
