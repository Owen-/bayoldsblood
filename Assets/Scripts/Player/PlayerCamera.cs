﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class PlayerCamera : MonoBehaviour 
{
	public bool shouldDebug = false;
	[Header("Mouse Settings")]
	public float MaxY = 8f;						// this is a global camera max position, not an angle or anything
	public float MinY = 2f;						// this is a global camera min position, not an angle or anything
	public float XSensitivity = 1f;
	public float YSensitivity = 1f;

	[Header("Camera")]
	public bool playerAlwaysVisible = true; 	// for advanced controls defining if the camera moves
	public bool shouldMoveWithPlayer = true;

	private float initialDistance; 				// this is the initial distance camera is from target
	public Vector3 viewTarget;					// central player point 
	private Vector3 desiredPosition; 			// where camera goes when nothing is obstructing
	private Vector3 forcedPosition;				// where camera goes when something is obstructing
	//private PlayerController playerController;
	private Player player;

	private Vector3 desiredDisplacement
	{
		get
		{
			return desiredPosition-viewTarget;
		}
		set 
		{
			desiredPosition = value+viewTarget;
		}
	}
		

	public void StartFollowing(Player p)
	{
		player = p;
		//playerController = player.GetPlayerController();
		viewTarget = player.GetCentre();

		initialDistance = (transform.position-viewTarget).magnitude;
		desiredPosition = transform.position;
		forcedPosition = desiredPosition;

	}

	private void Update() 
	{
		SetDesiredPosition();
		MoveToForcedPosition();
		RotatePlayer();
	}
	
	private void SetDesiredPosition() 	//Sets the desired position for the camera to be at assuming nothing obscuring player
	{
		float delta = Input.GetAxis("Mouse X")*XSensitivity;
		if(delta != 0) desiredDisplacement=Quaternion.Euler(0, delta, 0) *(desiredDisplacement); 

		if (shouldMoveWithPlayer)
		{
			if ((desiredDisplacement.magnitude > initialDistance)) 
				desiredPosition = Vector3.Lerp(desiredPosition, viewTarget + desiredDisplacement.normalized*initialDistance, 0.1f);
		}

		delta = Input.GetAxis("Mouse Y")*YSensitivity;
		desiredPosition.y = Mathf.Clamp(Mathf.Lerp(desiredPosition.y, desiredPosition.y-delta, 0.1f), MinY, MaxY);	


	}

	//Requires all traps and players etc to ignore raycast (or to cast on a separate layer!)
	private void MoveToForcedPosition()	//Casts a ray from the player to the camera. If a "structure" is obscuring then move to the point on the structure the ray hits.
	{
		RaycastHit hit;
		forcedPosition=desiredPosition;

		//if (Physics.Raycast(viewTarget, desiredDisplacement, out hit, initialDistance, 1<<8))
		if (Physics.Raycast(viewTarget, desiredDisplacement, out hit, initialDistance))
		{
			if (shouldDebug) Debug.DrawLine(viewTarget, desiredDisplacement, Color.green, initialDistance, true);
			if(playerAlwaysVisible) forcedPosition=hit.point;
		}

		transform.position=forcedPosition;
	}

	private void RotatePlayer()
	{
		viewTarget = player.GetCentre();
		transform.LookAt(viewTarget);
		//playerController.RotateTo(viewTarget-desiredDisplacement);
	}
}