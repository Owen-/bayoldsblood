﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour 
{
	public PlayerCamera PC;
	private void Start()
	{
		InvokeRepeating("CheckGround", 0.5f, 1f);
		PC.StartFollowing(this);
	}

	private void CheckGround()
	{
		RaycastHit hit;

        if (Physics.Raycast(transform.position, -Vector3.up, out hit))
        {
        	transform.position=hit.point;
        	CancelInvoke();
        }
	}

	public Vector3 GetCentre()
	{
		return transform.position;
	}
}
