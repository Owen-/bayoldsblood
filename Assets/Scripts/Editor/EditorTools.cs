﻿using UnityEngine;
using UnityEditor;


//showlist still requires the following in OnInspectorGUI

// serializedObject.Update()
// ShowList(serializedObject.FindProperty("propertynamehere"))
// serializedObject.ApplyModifiedProperties();


namespace EditorTools
{
	public static class Layout
	{
		public static void ShowList (SerializedProperty list) 
		{
			EditorGUILayout.PropertyField(list);
			EditorGUI.indentLevel++;
			if(list.isExpanded)
			{
				EditorGUILayout.PropertyField(list.FindPropertyRelative("Array.size"));
				for (int i = 0; i < list.arraySize; i++) 
				{
					EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i));
				}
			}
			EditorGUI.indentLevel--;
		}

		public static void ShowFloat (ref float field, string title, int space)
		{
			GUILayout.BeginHorizontal();
			GUILayout.Space(space);
			field = EditorGUILayout.FloatField(title, field);
			GUILayout.EndHorizontal();
		}

		public static void ShowInt (ref int field, string title, int space)
		{
			GUILayout.BeginHorizontal();
			GUILayout.Space(space);
			field = EditorGUILayout.IntField(title, field);
			GUILayout.EndHorizontal();
		}

		public static bool ShowBool(ref bool field, string title)
		{
			field = EditorGUILayout.Toggle(title, field);

			if (field) return true;
			else return false;
		}
	}
}
