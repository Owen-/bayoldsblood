﻿using System;
using UnityEngine;
using UnityEditor;
using System.Collections;

public class ProceduralTerrainMenuItems
{
	[MenuItem("Assets/Create/ProceduralTerrain/Biome")]
	public static void CreatBiome()
	{
		ScriptableObjectUtility.CreateAsset<Biome>();
	}
	
	[MenuItem("Assets/Create/ProceduralTerrain/TerrainInfo")]
	public static void CreatTerrainInfo()
	{
		ScriptableObjectUtility.CreateAsset<TerrainInfo>();
	}


}
