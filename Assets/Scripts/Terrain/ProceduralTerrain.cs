﻿using UnityEngine;
using System.Collections;

//Tidy code and make editor script
//Apply biome variations to terrainInfo.heightmap and splatmap
//still the occasional shitty remeshing thing

public class ProceduralTerrain : MonoBehaviour 
{
	private Transform player;
	private Terrain terrain;
	private TerrainData terrainData;
	private TerrainInfo terrainInfo;
	private bool isInitialized;
	private Texture2D[] terrainTextures;
	private GameObject[] terrainDetails;

	private float[,] heights;
	private int[,] details;

	public void Initialize(TerrainInfo TI, Transform _player)
	{
		terrainInfo=TI;
		terrain = GetComponent<Terrain>();
		terrainData = terrain.terrainData;
		player=_player;
		

		int detailNum = 0;

		terrainTextures=new Texture2D[TI.biomes.Length*3 +1];
		terrainTextures[0] = TI.mainTexture;

		for (int i =0; i<TI.biomes.Length; i++)
		{
			detailNum+=TI.biomes[i].biomeInfo.detailObjects.Length;
			TI.biomes[i].index =i*3+1;
			terrainTextures[i*3+1] 	= TI.biomes[i].biomeInfo.mainGroundTex;
			terrainTextures[i*3+2] 	= TI.biomes[i].biomeInfo.cliffTex;
			terrainTextures[i*3+3] 	= TI.biomes[i].biomeInfo.pathTex;
		}

		terrainDetails=new GameObject[detailNum+1];

		for (int i =0; i<TI.biomes.Length; i++)
		{
			for (int j = 0; j< TI.biomes[i].biomeInfo.detailObjects.Length; j++)
			{
				terrainDetails[i*j+j] = TI.biomes[i].biomeInfo.detailObjects[j];
			}
		}

		heights = new float[TI.resolution,TI.resolution];

		isInitialized=true;

	}

	private void SetSplats()
	{
		SplatPrototype[] tex = new SplatPrototype[terrainTextures.Length];

		for(int i = 0; i<terrainTextures.Length; i++)
		{
			tex[i] = new SplatPrototype();
			tex[i].texture = terrainTextures[i];
			tex[i].tileSize = new Vector2(1,1);
		}

		terrainData.splatPrototypes = tex;
	}

	private void SetDetails()
	{
		//DetailPrototype[] dets = new DetailPrototype[terrainDetails.Length];
		DetailPrototype[] dets = new DetailPrototype[1];
		dets[0] = new DetailPrototype();
		dets[0].renderMode = DetailRenderMode.Grass;
        dets[0].prototypeTexture = terrainInfo.mainGrass;

		//for (int i =0; i<dets.Length; i++)
		//{
		//	dets[i] = new DetailPrototype();
//
		//}

		terrainData.detailPrototypes = dets;
	}

	private void OnEnable()
	{
		if(isInitialized)
		{
			transform.position= new Vector3Step(player.position.x-(terrainInfo.size/2f), 0f, player.position.z-(terrainInfo.size/2f), terrainInfo.worldStep).ToVector3();
			//transform.position= new Vector3Step(player.position.x, 0f, player.position.z, step).ToVector3();
			terrain.terrainData = GenerateTerrainData();
		}
	}

	private TerrainData GenerateTerrainData()
	{
		int res = terrainInfo.resolution;

		terrainData.heightmapResolution=res;
		terrainData.alphamapResolution=res;
		terrainData.SetDetailResolution(res, 16);
		
		heights = terrainData.GetHeights(0, 0, res, res);
		details = terrainData.GetDetailLayer(0,0,res,res,0);
		SetSplats();
		SetDetails();
		float[,,] alphamaps = terrainData.GetAlphamaps(0,0,res,res);

		for (int i=0; i<res; i++) 
			for (int j=0; j<res; j++)
				Noise.ModifyTerrain(i, j, transform.position, terrainInfo, ref heights, ref alphamaps, ref details);

		terrainData.SetHeights(0,0, heights);
		terrainData.SetAlphamaps(0,0, alphamaps);
		terrainData.SetDetailLayer(0,0,0,details);

		terrainData.size= new Vector3(terrainInfo.size, terrainInfo.height, terrainInfo.size);
		return terrainData;
	}

	
}
