﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Vector3Step 
{
	public float x, y, z, s;
	public Vector3Step(float X, float Y, float Z, float S)
	{
		if(S <=0f) return;
		s=S;
		x=StepFloor(X);
		y=StepFloor(Y);
		z=StepFloor(Z);
	}

	public Vector3Step(Vector3 v, float S)
	{
		if(S <=0f) return;
		s=S;
		x=StepFloor(v.x);
		y=StepFloor(v.y);
		z=StepFloor(v.z);
	}

	public Vector3 ToVector3()
	{
		return new Vector3(x, y, z);
	}

	private float StepFloor(float f)
	{
		if(s <=0f) { Debug.Log(" NO STEP SPECIFIED!!"); return 0f; }
		float result = 0f;
		if(f>0) for(float i = 0f; i<=f; i+=s) result=i;
		else for(float i=0f; i>=f; i-=s) result =i;
		return result;
	}

	public static float StepFloor(float f, float step)
	{
		if(step <=0f) { Debug.Log(" NO STEP SPECIFIED!!"); return 0f; }
		float result = 0f;
		if (f > 0) for(float i = 0f; i<=f; i+=step) result=i;
		else for(float i = 0f; i>=f; i-=step) result=i;
		return result;
	} 
}
