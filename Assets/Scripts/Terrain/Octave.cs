﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Octave 
{
	public float a,f;
	public Octave(float amplitude, float frequency)
	{
		a=amplitude;
		f=frequency;
	}
}
