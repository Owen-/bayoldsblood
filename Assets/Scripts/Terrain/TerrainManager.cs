﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// manages the 2 proceduralterrains by simply enabling and disabling them 
// at appropriate times

public class TerrainManager : MonoBehaviour 
{
	public Transform player;
	public TerrainInfo terrainInfo;

	private ProceduralTerrain terrainA;
	private ProceduralTerrain terrainB;
	private Vector3Step lastPos;

	private void Start () 
	{
		terrainA = Terrain.CreateTerrainGameObject(new TerrainData()).AddComponent<ProceduralTerrain>();
		terrainB = Terrain.CreateTerrainGameObject(new TerrainData()).AddComponent<ProceduralTerrain>();
			
		terrainA.Initialize(terrainInfo, player); //assigns all the variables intiially
		terrainB.Initialize(terrainInfo, player); 

		terrainA.gameObject.SetActive(false);
		terrainB.gameObject.SetActive(false);

		lastPos = new Vector3Step(player.position.x, 0f, player.position.z, terrainInfo.worldStep);

		ToggleTerrain();
	}

	private void Update () 
	{
		if (PlayerHasMoved()) ToggleTerrain();
	}

	private void ToggleTerrain()
	{
		bool b = terrainA.gameObject.activeInHierarchy;
		terrainA.gameObject.SetActive(!b);
		terrainB.gameObject.SetActive(b);
	}

	private bool PlayerHasMoved()
	{
		float deltaX = player.position.x-lastPos.x;
		float deltaZ = player.position.z-lastPos.z;

		if (deltaX > terrainInfo.checkDistance || deltaX <-terrainInfo.checkDistance || deltaZ > terrainInfo.checkDistance || deltaZ < -terrainInfo.checkDistance) 
		{
			lastPos = new Vector3Step(player.position.x, 0f, player.position.z, terrainInfo.worldStep);
			return true;
		}
		else return false;
	}
}
