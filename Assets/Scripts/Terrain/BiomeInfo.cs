﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class BiomeInfo
{
	public Texture2D mainGroundTex;
	public Texture2D cliffTex;
	public Texture2D pathTex;
	public Texture2D wallsTex;
	public Texture2D specialTex;
	public GameObject[] detailObjects;
	public Octave[] octaves;
	public Vector3 origin;
	public float radius;
	public float influenceRadius;
	[Range(0,1)] public float cliffAngle = 0.5f;
	//public DetailPrototype[] details;
}
