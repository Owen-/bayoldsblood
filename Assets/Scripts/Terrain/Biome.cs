﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Biome : ScriptableObject
{
	public BiomeInfo biomeInfo;
	public int index ;

	public bool CanInfluence(Vector3 point, float r)
	{
		return (Vector3.Magnitude(point-biomeInfo.origin)<=r);
	}

	//returns float from 0 to 1
	public float GetInfluence(float x, float z)
	{
		Vector3 point = new Vector3(x, 0f, z);

		if(CanInfluence(point, biomeInfo.radius)) return 1f;
	
		if(CanInfluence(point, biomeInfo.influenceRadius))
		{
			float influence = Vector3.Magnitude(point-biomeInfo.origin)-biomeInfo.radius;

			influence/=(biomeInfo.influenceRadius-biomeInfo.radius);
			influence=1f-influence;

			return influence;
		}

		return 0f;
	}
}
