﻿using UnityEngine;
using System.Collections;


//producing symmetrical patterns
public static class Noise
{
	private static float staticOffset = 2000f;
	//public static void GetPerlinAt(int i, int j, Vector3 offset, float worldStep, Octave[] octaves, ref float[,] heights, ref float[,,] alphamaps)
	public static void ModifyTerrain(int i, int j, Vector3 offset, TerrainInfo TI, ref float[,] heights, ref float[,,] alphamaps, ref int[,] details)
	{
		float heightResult = 0f;
		float biomeInfluence = 0f;
		float biomeIndex = 0f;

		//Debug.Log(alphamaps.GetLength(2));
		float x = (i*TI.worldStep)+(offset.z);//+staticOffset); 
		float y = (j*TI.worldStep)+(offset.x);//+staticOffset);     //// do we really need to comment this out?

		float rand = Random.Range(0,1000);

		if (rand >= 900) details[i,j] = 1;
		else details[i,j]=0;

		foreach (Octave O in TI.octaves) heightResult += Mathf.PerlinNoise(x * O.f, y* O.f)*O.a;

		foreach (Biome B in TI.biomes)
		{
			//if (i==50 && j == 50) Debug.Log(x + " " + y + " " + alphamaps.GetLength(2));
			biomeInfluence = B.GetInfluence(y,x); ///..... why is this whats workinng? 
			float biomeheightResult = 0f;
			
			if(biomeInfluence > 0) 
			{
				if(Random.value < biomeInfluence) details[i,j]=0;
				foreach (Octave O in B.biomeInfo.octaves) biomeheightResult += Mathf.PerlinNoise(x * O.f, y* O.f)*O.a;

				alphamaps[i,j,0] = (1f-biomeInfluence);

				alphamaps[i,j, B.index] = biomeInfluence;
			}

			heightResult = heightResult*(1-biomeInfluence) + biomeheightResult*biomeInfluence;
			biomeIndex++;
			
		}


		heights[i,j]=heightResult/TI.octaves.Length;

		
	}
}



	//private float GeneratePerlinHeight(float i, float j)
	//{
//
	//	float worldZ = i;
	//	float worldX = j;
	//	if (worldZ > terrainInfo.worldSize || worldZ < -terrainInfo.worldSize || worldX > terrainInfo.worldSize || worldX < -terrainInfo.worldSize) return 0f;
//
	//	float scale = terrainInfo.defaultScale;
	//	float wavelength = terrainInfo.defaultWavelength;
//
//	//	foreach (Biome B in terrainInfo.biomes) B.Process(worldX, worldZ, ref scale, ref wavelength);
//
	//	worldX/=wavelength;
	//	worldZ/=wavelength;
//
	//	float heightResult = scale*Mathf.PerlinNoise(worldX, worldZ);
//
	//	worldX*=2f;
	//	worldZ*=2f;
//
	//	heightResult += (scale*.5f)*Mathf.PerlinNoise(worldX, worldZ);
	//	heightResult/=2;
//
	//	return heightResult;
//
	//}
//
	//private float GenerateAlphamap(int a, int i, int j)
	//{
	//	float worldZ = (i/(float)(res-1))*terrainInfo.size+transform.position.z; // consider the block 3x0 pixels in (on 1st terrain), in 1 unity space difference between 2 scales
	//	float worldX = (j/(float)(res-1))*terrainInfo.size+transform.position.x;
//
	//	float influence = 0f;
	//	if(a==0) influence = 1f;
	//	//foreach (Biome B in terrainInfo.biomes) B.ProcessAlphaMap(worldX, worldZ, ref influence);
//
	//	return influence;
	//}
