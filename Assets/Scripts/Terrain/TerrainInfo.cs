﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class TerrainInfo : ScriptableObject
{
	public int size, height;
	[Range(5,11)] public int resolutionPower;
	public float defaultWavelength;
	public float defaultScale;
	public float worldSize;
	public float maxCheckDistance;
	public Texture2D mainTexture;
	public Texture2D mainGrass;
	public Biome[] biomes;
	public Octave[] octaves;

	private float _worldStep;
	private int _resolution;

	public float checkDistance
	{
		get
		{
			float result = Vector3Step.StepFloor(maxCheckDistance, worldStep);
			if(result <= 0f) result = worldStep;
			return result;
		}
	}

	public int resolution
	{
		get
		{
			if(_resolution <= 0) _resolution = (int) Mathf.Pow(2, resolutionPower)+1;
			return _resolution;
		}
	}

	public float worldStep
	{
		get
		{
			if(_worldStep <= 0f) _worldStep = size/(float)(resolution-1);
			return _worldStep;
		}
	}
}

